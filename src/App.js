import React, {useState} from 'react'
import {
  OneColumn,
  TwoColumn,
  FourColumn
} from './components/GridSystem'
import {questions} from './database/Questions'

export const App = ()=> {
  let API_URL_LOCAL = 'http://localhost:9596'
  let API_URL = 'https://oslq-test.herokuapp.com/'

  const [question0, setQuestion0] = useState('')
  const [question1, setQuestion1] = useState('')
  const [question2, setQuestion2] = useState('')
  const [question3, setQuestion3] = useState('')
  const [question4, setQuestion4] = useState('')
  const [question5, setQuestion5] = useState('')
  const [question6, setQuestion6] = useState('')
  const [question7, setQuestion7] = useState('')
  const [question8, setQuestion8] = useState('')
  const [question9, setQuestion9] = useState('')
  const [question10, setQuestion10] = useState('')
  const [question11, setQuestion11] = useState('')
  const [question12, setQuestion12] = useState('')
  const [question13, setQuestion13] = useState('')
  const [question14, setQuestion14] = useState('')
  const [question15, setQuestion15] = useState('')
  const [question16, setQuestion16] = useState('')
  const [question17, setQuestion17] = useState('')
  
  const radioOption = (event, id)=>{
    switch(id){
      case 0:
        setQuestion0(event)
      break
      case 1:
        setQuestion1(event)
      break
      case 2:
        setQuestion2(event)
      break
      case 3:
        setQuestion3(event)
      break
      case 4:
        setQuestion4(event)
      break
      case 5:
        setQuestion5(event)
      break
      case 6:
        setQuestion6(event)
      break
      case 7:
        setQuestion7(event)
      break
      case 8:
        setQuestion8(event)
      break
      case 9:
        setQuestion9(event)
      break
      case 10:
        setQuestion10(event)
      break
      case 11:
        setQuestion11(event)
      break
      case 12:
        setQuestion12(event)
      break
      case 13:
        setQuestion13(event)
      break
      case 14:
        setQuestion14(event)
      break
      case 15:
        setQuestion15(event)
      break
      case 16:
        setQuestion16(event)
      break
      case 17:
        setQuestion17(event)
      break
    }
  }
  const handleSubmit = async(event)=>{
    event.preventDefault()
    const newTest = JSON.stringify({
      question0,
      question1,
      question2,
      question3,
      question4,
      question5,
      question6,
      question7,
      question8,
      question9,
      question10,
      question11,
      question12,
      question13,
      question14,
      question15,
      question16,
      question17
    })
    const options = {
      method:'post',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json'
      },
      body:newTest
    }
    try {
      const response = await fetch(`${API_URL}/api/test`, options)
      if(response.ok){
        alert('Gracias por tu participación')
        window.location.replace('https://oslq-test.herokuapp.com/')
        return response 
      }
    } catch (error) {
      alert(`No se pudo realizar el registro debido a ${error}`)
    }
  }
  return (
    <div className="container-fluid">
      {/* <OneColumn/>
      <TwoColumn/>
      <FourColumn/> */}
      <h1 className='text-center mt-4'>Cuestionario de Autorregulación del Aprendizaje Online OSLQ</h1>
      <div className='container '>
        <p className='lead text-center'>En esta parte debería ir una descripción de la prueba</p>
        <form onSubmit={handleSubmit}>
          <div className='col-sm mt-5'>
            <h4 className='mb-3'>Habilidades Metacognitivas</h4>
            <div className='form-control '>
              {questions.map((question)=>(
                <div key={question.id}>
                  <label className='form-label'>{question.title}</label>
                  <br/>
                  <div className='form-check form-check-inline'>
                    <input 
                      type='radio'
                      value='1' 
                      className='form-check-input'
                      onChange={event => radioOption(event.target.value, question.id)} 
                      name={question.number}
                      id={`radio1_question`+ question.number} required/>
                    <label
                      className='form-check-label' 
                      htmlFor={`radio1_question`+ question.number}>
                        1
                    </label>
                  </div>
                <div className='form-check form-check-inline'>
                  <input 
                    type='radio'
                    value='2'
                    className='form-check-input'
                    onChange={event => radioOption(event.target.value, question.id)} 
                    name={question.number}
                    id={`radio2_question`+ question.number} />
                  <label
                    className='form-check-label' 
                    htmlFor={`radio2_question`+ question.number}>
                      2
                  </label>
                </div>
                <div className='form-check form-check-inline'>
                  <input 
                    type='radio'
                    value='3'
                    className='form-check-input'
                    onChange={event => radioOption(event.target.value, question.id)} 
                    name={question.number}
                    id={`radio3_question`+ question.number} />
                  <label 
                    className='form-check-label' 
                    htmlFor={`radio3_question`+ question.number}>
                      3
                  </label>
                </div>
                <div className='form-check form-check-inline'>
                  <input 
                    type='radio'
                    value='4'
                    className='form-check-input'
                    onChange={event => radioOption(event.target.value, question.id)} 
                    name={question.number}
                    id={`radio4_question`+ question.number} />
                  <label
                    className='form-check-label' 
                    htmlFor={`radio4_question`+ question.number}>
                      4
                  </label>
                </div>
                <div className='form-check form-check-inline'>
                  <input 
                    type='radio'
                    value='5'
                    className='form-check-input'
                    onChange={event => radioOption(event.target.value, question.id)} 
                    name={question.number}
                    id={`radio5_question`+ question.number} />
                  <label
                  className='form-check-label' 
                  htmlFor={`radio5_question`+ question.number}>
                    5
                  </label>
                </div>
                <br/>
                </div>
              ))}
            <div className="d-grid mt-3 col-6 mx-auto">
              <button type="submit" className="fs-4 btn btn-primary" >Enviar</button>
            </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  )
}
