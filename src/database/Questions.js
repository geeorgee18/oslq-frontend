export const questions = [
    {
        id:0,
        number:'1',
        title:'1.	Pienso en lo que realmente necesito aprender antes de comenzar una tarea en este curso en línea.'
    },
    {
        id:1,
        number:'2',
        title:'2.	Me hago preguntas sobre lo que voy a estudiar antes de empezar a aprender para este curso en línea.'
    },
    {
        id:2,
        number:'3',
        title:'3.	Me fije objetivos a corto plazo (diarios o semanales) así como objetivos a largo plazo (mensuales o para todo el curso en línea)'
    },
    {
        id:3,
        number:'4',
        title:'4.	Me fijé metas para ayudarme a manejar mi tiempo de estudio para este curso en línea.'
    },
    {
        id:4,
        number:'5',
        title:'5.	Me he fijado objetivos específicos antes de empezar una tarea en este curso online.'
    },
    {
        id:5,
        number:'6',
        title:'6.	Pienso en formas alternativas de resolver un problema y elegir la mejor para este curso en línea.'
    },
    {
        id:6,
        number:'7',
        title:'7.	Trato de usar estrategias en este curso en línea que han funcionado en el pasado.'
    },
    {
        id:7,
        number:'8',
        title:'8.	Tengo un propósito específico para cada estrategia que uso en este curso online.'
    },
    {
        id:8,
        number:'9',
        title:'9.	Soy consciente de las estrategias que uso cuando estudio para este curso en línea.'
    },
    {
        id:9,
        number:'10',
        title:'10.	Aunque no tenemos que asistir a clases diarias, trato de distribuir mi tiempo de estudio para este curso en línea de manera uniforme a través de los días.'
    },
    {
        id:10,
        number:'11',
        title:'11.	Reviso periódicamente para ayudarme a entender las relaciones importantes en este curso en línea.'
    },
    {
        id:11,
        number:'12',
        title:'12.	Me encuentro haciendo pausas regularmente para comprobar mi comprensión de este curso en línea.'
    },
    {
        id:12,
        number:'13',
        title:'13.	Me hago preguntas sobre lo bien que me va mientras aprendo algo en este curso en línea'
    },
    {
        id:13,
        number:'14',
        title:'14.	Pienso en lo que he aprendido después de terminar de trabajar en este curso en línea.'
    },
    {
        id:14,
        number:'15',
        title:'15.	Me pregunto qué tan bien logré mis metas una vez que termine de trabajar en este curso en línea.'
    },
    {
        id:15,
        number:'16',
        title:'16.	Cambio de estrategia cuando no avanzo mientras aprendo para este curso en línea.'
    },
    {
        id:16,
        number:'17',
        title:'17.	Me encuentro analizando la utilidad de las estrategias mientras estudio para este curso en línea'
    },
    {
        id:17,
        number:'18',
        title:'18.	Me pregunto si hay otras formas de hacer las cosas después de que termine de aprender para esto en línea.'
    }
]