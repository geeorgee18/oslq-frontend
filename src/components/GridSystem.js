import React from 'react';

export const OneColumn = () => {
    return (
        <div className='row'>
            <div className='col-sm-12' style={{backgroundColor:'orangered'}}>
                <h1 className='fs-1 text-center'>100%</h1>
            </div>
        </div>
    )
}
export const TwoColumn = () => {
    return (
        <div className='row'>
            <div className="fs-2 col-sm-6 text-center" style={{backgroundColor:'yellow'}}>50%</div>
            <div className="fs-2 col-sm-6 text-center" style={{backgroundColor:'orange'}}>50%</div>
        </div>
    )
}
export const FourColumn = () => {
    return (
        <div className='row'>
            <div className="fs-3 col-sm text-center" style={{backgroundColor:'skyblue'}}>25%</div>
            <div className="fs-3 col-sm  text-center" style={{backgroundColor:'paleturquoise'}}>25%</div>
            <div className="fs-3 col-sm  text-center" style={{backgroundColor:'skyblue'}}>25%</div>
            <div className="fs-3 col-sm  text-center" style={{backgroundColor:'paleturquoise'}}>25%</div>
        </div>
    )
}